
-- SUMMARY --

The easysitemap module helps to create, update, view your sitemap.xml 
of your website.

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for 
further information.
